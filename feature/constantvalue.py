#!/usr/bin/env python
import numpy as np

class constantvaluelength:
    """
    The :py:func:`constantvaluelength` class counts the number of recent samples that have the same value as the last value.
    
    Parameters
    ----------
    
    Returns
    -------
    constantvaluelength : Instance of the :py:func:`constantvaluelength` class
    
    """
    
    def __init__(self):
        """Create an instance of the :py:func:`constantvaluelength` class"""
        self.rLastValue = np.nan
        self.iCount  = np.nan
        
        return

    def __call__(self,NewValue):
        """Update constant value length
        
        The purpose of :py:func:`constantvaluelength.__call__` is to 
        (a) store the last known value and the number of recently processed samples with this value
        (b) produce the number of recently processed samples with value equal to the last known value
        
        Parameters
        ----------
        NewValue : Last known sensor value
    
        Returns
        -------
        NumberOfConstantSamples : Number of recently processed samples with value equal to the last known value
        
        """
        
        rNewValue = NewValue # add 'r' prefix - naming convention for real numbers
        
        rLastValue = self.rLastValue
        iCount     = self.iCount
        
        xAcceptNewValue = np.isreal(rNewValue) and (not np.isnan(rNewValue))
        if xAcceptNewValue:
            
            xAcceptLastValue = np.isreal(rLastValue) and (not np.isnan(rLastValue))
            
            if xAcceptLastValue:
                if rNewValue==rLastValue:
                    iCount += 1 # If the new real value is the same, update counter
                else:
                    iCount = 0 # If the new real value is different, reset counter to zero
            else:
                iCount = 0 # As long as there is no recorded real value, produce counter equal to zero
                
            # update last known value in memory with current value
            self.rLastValue = rNewValue
        else:
            pass # count stays where it is
        
        self.iCount = iCount
        NumberOfSamples = iCount
        
        return NumberOfSamples