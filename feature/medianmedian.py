#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time

def medianmedianlinefit(X,Y):
    """
    The :py:func:`medianmedianlinefit` function fits a median-median line to a set of data pairs (x_i,y_i) provided as two vectors X and Y. The data point corresponding the median value in X is used as a test sample. The remaining samples are the training samples.
    
    Parameters
    ----------
    X : Values for independent variable (e.g., time)
    Y : Values for dependent variable (e.g., sensor value)
    
    Returns
    -------
    EstimatedValue : Value of the fitted line as the median value in X
    EstimatedSlope : Value of the line slope
    TestError : Deviation between EstimatedValue and Y corresponding to the median value in X
    MedianAbsoluteDeviation : Median absolute deviation (MAD) between calibration data and fitted line
    
    """
    
    # type conversion + apply naming convention
    rX = X
    rY = Y
    
    iNumberOfX = len(rX)
    iNumberOfY = len(rY)
    
    if iNumberOfX==iNumberOfY:
        if np.mod(iNumberOfX,3)==0:
            
            iSortX  = np.argsort(rX)
            rX = rX[iSortX]
            rY = rY[iSortX]
            
            iCenterIndex  =int((iNumberOfX-1)/2)
            rXcenter = rX[iCenterIndex]
            rX = rX-rXcenter
            
            iMedianWindow = int(iNumberOfX/3)
            
            Xmed = np.ones(3)*np.nan
            Ymed = np.ones(3)*np.nan
            for iWindow in range(3):
                iSamples = iWindow*iMedianWindow+np.arange(iMedianWindow)
                x_ = rX[iSamples]
                y_ = rY[iSamples]
                xSelect = np.logical_not(x_==0)
                y_ = y_[xSelect]
                x_ = x_[xSelect]
                Xmed[iWindow] = np.median(x_)
                Ymed[iWindow] = np.median(y_)
                
            rSlope = (Ymed[2]-Ymed[0])/(Xmed[2]-Xmed[0])
            rIntercept = np.mean(Ymed-rSlope*Xmed)
            rDeviation = rY[iCenterIndex]-rIntercept
            
            xSelect = np.logical_not(rX==0)
            rYhat = rX[xSelect]*rSlope+rIntercept
            rAbsDev = np.abs(rY[xSelect]-rYhat)
            rMedianAbsDev = np.median(rAbsDev)
            
        else:
            raise SyntaxError('ERROR - length of X must be divisible by 3')
    else:
        raise SyntaxError('ERROR - X and Y must have same length')
    
    # reverse naming convention
    EstimatedValue = rIntercept
    EstimatedSlope = rSlope
    TestError = rDeviation
    MedianAbsoluteDeviation = rMedianAbsDev
    
    return EstimatedValue, EstimatedSlope, TestError, MedianAbsoluteDeviation

class medianmedianlineplot:
    
    def __init__(self,medmedline,\
                 iDisplayInterval=0,\
                 iDisplayHorizon=0):
        
        """Create an instance of a point-wise productivity check for datetime"""
        dfStack = pd.DataFrame(columns=['x','y'])
        
        self.dfStack = dfStack
        self.iMedianWindow = iMedianWindow
        self.iDisplayInterval = iDisplayInterval
        self.iNumberTestSamples = 0;
        
        iDisplayHorizon = np.max([iDisplayHorizon,self.iMedianMedianWindow]) 
        
        self.iDisplayHorizon = iDisplayHorizon
        
        iDisplayHorizon = np.max([iDisplayHorizon,self.iMedianMedianWindow]) 
        
        self.iDisplayHorizon = iDisplayHorizon
        
        if iDisplayInterval>0:
            
            hFig,hAx = plt.subplots(1,1)
            
            hAx.plot(dfStack['x'],dfStack['y'],color='k',marker='.',linestyle='None')
            hAx.plot(dfStack['x'],dfStack['y'],color='m',marker='o',linestyle='None')
            hAx.plot(dfStack['x'],dfStack['y'],color='b',marker='None',linestyle='-')
            hAx.plot(dfStack['x'],dfStack['y'],color='r',marker='+',linestyle='-')
            
            hFig.canvas.draw()
            time.sleep(0.001)
            
            self.hFig       = hFig
            self.hAx        = hAx
            
        return
    
class medianmedianline:
    """
    The :py:func:`medianmedianline` class fit a median-median line in a moving-window fasion to a univariate data series.
    
    Parameters
    ----------
    
    Returns
    -------
    medianmedianline : Instance of the :py:func:`medianmedianline` class
    
    """
    
    def __init__(self,iMedianWindow=3):
        """Create an instance of the :py:func:`medianmedianline` class"""
        
        if np.mod(iMedianWindow,2)==1:
            dfStack = pd.DataFrame(columns=['x','y'])
        
            self.dfStack = dfStack
            self.iMedianWindow = iMedianWindow
            
        else:
            raise SyntaxError('ERROR - iMedianWindow must be odd')
            
        return
    
    def __call__(self,NewValue,NewTime):
        """Update moving-window median-median line
        
        Parameters
        ----------
        NewValue : Last known sensor value
    
        Returns
        -------
        NumberOfConstantSamples : Number of recently processed samples with value equal to the last known value
        
        """
        # apply naming conventions:
        rNewValue = NewValue
        dtNewTime = NewTime
        
        xAcceptNewValue = np.isreal(rNewValue) and (not np.isnan(rNewValue))
        xAcceptNewTime = (not np.isnat(dtNewTime))
        
        if (xAcceptNewValue and xAcceptNewTime ):
            # add values to stack
            dfStack = self.dfStack
            dfStackAdd    = pd.DataFrame([[dtNewTime,rNewValue]],columns=['x','y'])
            dfStack       = dfStack.append(dfStackAdd,sort=False)
            self.dfStack = dfStack
        
            if dfStack.shape[0]<self.iMedianMedianWindow:
                EstimatedValue = np.nan
                EstimatedSlope = np.nan
                TestError = np.nan
                MedianAbsoluteDeviation = np.nan
            else:
                dfStack.sort_values(by='x',inplace=True)
                dfStack = dfStack.iloc[-self.iMedianMedianWindow:,:]    
                X = (dfStack.x-dfStack.x.iloc[0]).astype('timedelta64[s]').values
                Y = dfStack.loc[:,'y'].values
                EstimatedValue, EstimatedSlope, TestError, MedianAbsoluteDeviation =medianmedianlinefit(X,Y)
        else:
            EstimatedValue = np.nan
            EstimatedSlope = np.nan
            TestError = np.nan
            MedianAbsoluteDeviation = np.nan
            
        return EstimatedValue, EstimatedSlope, TestError, MedianAbsoluteDeviation
    
    @property
    def iMedianMedianWindow(self):
        """Number of samples included to compute the median-median line"""
        iMedianMedianWindow = int(self.iMedianWindow*3)
        
        return iMedianMedianWindow
    
    @property
    def iCenterIndex(self):
        """Index of central sample in the stack of samples to compute and test the median-median line"""
        iCenterIndex = int(np.floor( (int(self.iMedianMedianWindow)-1)/2 ))
        return iCenterIndex
    