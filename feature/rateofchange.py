#!/usr/bin/env python
import numpy as np

class rateofchange:
    """
    The :py:func:`rateofchange` class is used to compute the point-wise rate-of-change of a signal.
    
    Parameters
    ----------
    
    Returns
    -------
    rateofchange : Instance of the :py:func:`constantvaluelength` class
    
    """
    
    def __init__(self):
        """Create an instance of the :py:func:`rateofchange` class"""
        self.rLastValue = np.nan
        self.dtLastTime = np.datetime64('NaT')
        
        return

    def __call__(self,NewValue,NewTime):
        """Compute rate of change
        
        The purpose of :py:func:`rateofchange.__call__` is to 
        (a) store the time and value of the last known sample
        (b) compute the rate-of-change, i.e. the change of value per time unit for pairs of consecutive samples
        
        Parameters
        ----------
        NewValue : Last known sensor value
        NewTime : Time of last known sensor value
    
        Returns
        -------
        Rate : Computed rate-of-change
        
        """
        
        # apply naming conventions:
        rNewValue = NewValue
        dtNewTime = NewTime
        
        xAcceptNewValue = np.isreal(rNewValue) and (not np.isnan(rNewValue))
        xAcceptNewTime = (not np.isnat(dtNewTime))
        
        if (xAcceptNewValue and xAcceptNewTime ):
            
            # read/check memory:
            rLastValue = self.rLastValue
            dtLastTime = self.dtLastTime
            xAcceptLastValue = np.isreal(rLastValue) and (not np.isnan(rLastValue))
            xAcceptLastTime = (not np.isnat(dtLastTime))
            
            # rate computation:
            if (xAcceptLastValue and xAcceptLastTime):
                delta = dtNewTime - dtLastTime
                totalsec = delta/np.timedelta64(1, 's')
                rRate = (rNewValue-rLastValue)/totalsec
                pass
            else:
                rRate = np.nan # last known values not of expected format -> set rate to NaN
            
            # memory update:
            self.rLastValue = rNewValue
            self.dtLastTime = dtNewTime
            
        else:
            rRate = np.nan # new values not of expected format -> set rate to NaN
        
        rRate = np.float64(rRate)
        
        # reverse naming conventions:
        Rate = rRate
        
        return Rate
    
class pointwisedifference:
    """
    The :py:func:`pointwisedifference` class is used to compute the difference between consecutive sample values.
    
    Parameters
    ----------
    
    Returns
    -------
    pointwisedifference : Instance of the :py:func:`pointwisedifference` class
    
    """
    
    def __init__(self):
        """Create an instance of the :py:func:`pointwisedifference` class"""
        self.rLastValue = np.nan
        
        return

    def __call__(self,NewValue):
        """Compute point-wise difference
        
        The purpose of :py:func:`pointwisedifference.__call__` is to 
        (a) store the last known sample value 
        (b) compute the point-wise difference, i.e. the change of value between consecutive samples
        
        Parameters
        ----------
        NewValue : Last known sensor value
    
        Returns
        -------
        Difference : Difference between last known value and previous value
        
        """
        
        # apply naming conventions:
        rNewValue = NewValue
        
        # Check new value
        xAcceptNewValue = np.isreal(rNewValue) and (not np.isnan(rNewValue))
        
        if xAcceptNewValue:
            # Read/check memory
            rLastValue = self.rLastValue
            xAcceptLastValue = np.isreal(rLastValue) and (not np.isnan(rLastValue))
            
            # Compute difference
            if xAcceptLastValue:
                rDiff = rNewValue-rLastValue
            else:
                rDiff = np.nan
            
            # Update memory
            self.rLastValue = rNewValue
            pass
        else:
            rDiff = np.nan
        
        rDiff = np.float64(rDiff)
        
        # reverse naming conventions:
        Difference = rDiff
        
        return Difference
    