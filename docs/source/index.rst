.. AnomalyToolKit documentation master file, created by
   sphinx-quickstart on Wed Apr 24 02:01:17 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AnomalyToolKit's documentation!
==========================================

.. automodule:: advanced.unsupervised.ppca
    :members:

.. automodule:: basic.production
    :members:

.. automodule:: basic.typical
    :members:

.. automodule:: basic.visual
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
