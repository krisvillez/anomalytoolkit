#!/usr/bin/env python

import calmap
import datetime
import matplotlib.colors as colors
import matplotlib.dates  as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy
import pandas

def makechartbar(x,y,*,Xlim,Ylim,Yticks,iTimeWindow,rTarget=numpy.nan,Ylabel=['']):
    """:py:func:`makechartbar` produces a bar chart.
    
    Parameters
    ----------
    x : array with independent variable (e.g. time)
    y : array with dependent variable (e.g. counts, score)
    Xlim : limits for X axis
    Ylim : limits for Y axis
    Yticks : ticks for Y axis
    iTimeWindow : Time length of time slot (applied during aggregation)
    rTarget : [optional] Target value
    Ylabel : [optional] Y label string
    
    Returns
    -------
    fig : Figure object
    ax : Axes object
    
    """
    
    fig = plt.figure(figsize=(4,2*(Xlim[1]-Xlim[0]).days*1/iTimeWindow),dpi=200)
    ax = fig.gca()
    
    colour = ['b','r','k']
    shift = 3-len(x) 
    for s in range(len(x)):
        ax.barh(x[s],y[s],height =iTimeWindow/24*4/5,edgecolor='none',color=colour[s+shift])
    plt.grid(True,axis='x')
    
    if (numpy.isnan(rTarget)):
        pass
    else:
        plt.plot((rTarget,rTarget),(Xlim[0],Xlim[1]),'b-')
        
    # X-axis
    ax.set_ylabel('Time slot [-]')
    ax.set_ylim((Xlim[0],Xlim[1] ))
    myFmt = mdates.DateFormatter('%Y.%m.%d')
    ax.yaxis.set_major_locator(ticker.MultipleLocator(base=1.0))
    ax.yaxis.set_major_formatter(myFmt)
    ax.invert_yaxis()
    
    # Y-axis
    ax.set_xlabel(Ylabel)
    ax.xaxis.tick_top()
    ax.xaxis.set_label_position('top') 
    plt.xticks(Yticks,rotation=90)
    ax.set_xlim((Ylim[0],Ylim[1]))
    
    return fig,ax

def makechartstem(x,y,*,Xlim,Ylim,Yticks,iTimeWindow,rTarget=numpy.nan,Ylabel=['']):
    """:py:func:`makechartstem` produces a stem plot.
    
    Parameters
    ----------
    x : array with independent variable (e.g. time)
    y : array with dependent variable (e.g. counts, score)
    Xlim : limits for X axis
    Ylim : limits for Y axis
    Yticks : ticks for Y axis
    iTimeWindow : Time length of time slot (applied during aggregation)
    rTarget : [optional] Target value
    Ylabel : [optional] Y label string
    
    Returns
    -------
    fig : Figure object
    ax : Axes object
    
    """
    
    fig = plt.figure(figsize=(2*(Xlim[1]-Xlim[0]).days*1/iTimeWindow,4),dpi=200)
    fig = plt.figure(dpi=200)
    ax = fig.gca()
    
    colour = ['b','r','k']
    shift = 3-len(x) 
    for s in range(len(x)):
        ax.stem(x[s],y[s], linefmt=colour[s+shift]+'-', markerfmt=colour[s+shift]+'.', basefmt='k-')
    plt.grid(True,axis='y')
    
    if (numpy.isnan(rTarget)):
        pass
    else:
        plt.plot((Xlim[0],Xlim[1]),(rTarget,rTarget),'b-') 
    
    # X-axis
    ax.set_xlabel('Time slot [-]')
    ax.set_xlim((Xlim[0],Xlim[1] ))
    myFmt = mdates.DateFormatter('%Y.%m.%d')
    ax.xaxis.set_major_locator(ticker.MultipleLocator(base=1.0))
    ax.xaxis.set_major_formatter(myFmt)
    plt.xticks(rotation=90)
    
    # Y-axis
    ax.set_ylim((Ylim[0],Ylim[1] ))
    ax.set_ylabel(Ylabel)
    plt.yticks(Yticks);
    
    return fig,ax
    
    
def makechartheat(data,*,mode='sequential',maximumvalue=1):
    """:py:func:`makechartheat` produces a calendar heat map.
    
    Parameters
    ----------
    data : pandas series object including time series with daily values
    mode : string specifying the type of color scheme that is used ('sequential' or 'divergent')
    maximumvalue : value which corresponds to the last color in the color scheme
    
    Returns
    -------
    fig : Figure object
    ax : Axes object
    
    """
    
    if mode=='sequential':
        cmap = mapsequential()
    elif mode=='divergent':
        cmap = mapdiverge()
    else:
        raise SyntaxError('ERROR - Unknown mode')
        
    fig,ax=calmap.calendarplot(data,cmap=cmap,vmin=0,vmax=maximumvalue ,how=None,
                    fillcolor='lightgrey',linecolor ='black', linewidth=1, daylabels='MTWTFSS',
                    fig_kws=dict(figsize=(12, 6)))
    
    return fig,ax

def mapdiverge():
    """:py:func:`mapdiverge` produces a colormap for use in heatmaps. It is color scheme that is designed to be colorblind friendly and is the one used by default for heatmaps requiring a diverging colour scheme.
    
    Parameters
    ----------
    
    Returns
    -------
    
    """
    
    cvals  = [0, 0.4, 0.5, 0.6, 1]
    colors255 = [(215,25,28),(253,174,97),(255,255,191),(171,217,233),(44,123,182)]
    colors1 = [[ x/255 for x in color1] for color1 in colors255 ]
    tuples = list(zip(cvals, colors1))
    cmap = colors.LinearSegmentedColormap.from_list("", tuples)
    
    return cmap

def mapsequential():
    """:py:func:`mapsequential` produces a colormap for use in heatmaps. It is color scheme that is designed to be colorblind friendly and is the one used by default for heatmaps requiring a sequential colour scheme.
    
    Parameters
    ----------
    
    Returns
    -------
    
    """
    
    cvals  = [0/7, 2/7, 4/7, 6/7 ,7/7]
    colors255 = [(0,0,0),(215,25,28),(253,174,97),(171,221,164),(43,131,186)]
    colors1 = [[ x/255 for x in color1] for color1 in colors255 ]
    tuples = list(zip(cvals, colors1))
    cmap = colors.LinearSegmentedColormap.from_list("", tuples)
    
    return cmap

def multiindex(df):
    """ Create dataframe with two-level index with 'Date' and 'Time' as first and second level from a dataframe with 'DateTime' as one of the columns.
    
    Parameters
    ----------
    df : Dataframe with 'DateTime' column containing timestamps (as numpy.datetime64)
    
    Returns
    -------
    df : Dataframe with 'Date' and 'Time' as two-level index
    
    """
    dtDate = pandas.to_datetime(df['DateTime']).dt.date
    dtTime = pandas.to_datetime(df['DateTime']).dt.time
    df.index = pandas.MultiIndex.from_arrays([dtDate,dtTime], names=['Date','Time'])
    return df
    
def oneindex(df,*,iTimeWindow=1):
    """ Create dataframe with datetime as single-level index from a dataframe with a two-level index with 'Date' and 'TimeSlot' as first and second level.
    
    Parameters
    ----------
    df : Dataframe with two-level index with levels: 'Date' and 'TimeSlot'
    iTimeWindow : Number of hours in a single time slot
    
    Returns
    -------
    dfout : Dataframe with time as single-level index
    
    """
    dfout = df.copy()
    date = df.index.get_level_values('Date')
    hour = (df.index.get_level_values('TimeSlot')+0.5)*iTimeWindow
    minute = numpy.round((hour-numpy.floor(hour))*60)
    hour = numpy.floor(hour)
    datehourminute = zip(date.values,hour, minute) 
    dfout.index = [ pandas.to_datetime(pandas.Timestamp(dhm[0]))+datetime.timedelta(hours=dhm[1],minutes=dhm[2]) for dhm in datehourminute ] ;
    
    return dfout