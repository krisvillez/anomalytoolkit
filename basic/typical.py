#!/usr/bin/env python

import datetime
import time
import matplotlib.dates  as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy
import pandas
from basic.visual import makechartbar
from basic.visual import makechartstem
from basic.visual import makechartheat
from basic.visual import multiindex
from basic.visual import oneindex
from basic.visual import mapsequential

def IntegratePWL(x,y,offset):
    """
    :py:func:`IntegratePWL` integrate a piece-wise linear (PWL) function defined by (x,y) coordinate pairs minus an offset.
    
    Parameters
    ----------
    x : ordinate values
    y : coordinate values
    offset : offset (minimal y value)
    
    Returns
    -------
    rIntegral : Integral value
    
    """
    
    xDenser = y>offset
    xZero= numpy.logical_xor(xDenser[:-1],xDenser[1:])
    index0=numpy.where(xZero)[0]
    index1=numpy.where(xZero)[0]+1
    rZero = x[index0]+(x[index1]-x[index0])*(offset-y[index0])/(y[index1]-y[index0])
    
    y = y-offset
    y = numpy.append(y,numpy.zeros(len(rZero),dtype='float')) 
    x = numpy.append(x,rZero)
    iSort = numpy.argsort(x)
    x = x[iSort]
    y = y[iSort]
    
    rMassAbove = (y[1:]+y[:-1])*(x[1:]-x[:-1])/2
    rMassBelow = offset*(x[1:]-x[:-1])
    rMassBelow[rMassAbove<0]=0
    rMassAbove[rMassAbove<0]=0
    rIntegral = numpy.sum(rMassAbove+rMassBelow)
    
    return rIntegral           

class aggregate:
    """
    :py:func:`aggregate` generates an instance of the aggregated production check. This aggregation is used to summarize and visualize the results of basic production checks (of the :py:func:`datetimecheck` and :py:func:`numericcheck` classes).
    
    Using the class
    ---------------
    
    Parameters
    ----------
    TimeWindow : The time window to aggregate over, expressed in hours. Must be an integer equal to 1, 2, 3, 4, 6, 8, 12, or 24.
    NumberOfSample : [optional] The number of samples expected within the specified time window.
    
    Returns
    -------
    aggregate : Object describing the aggregated data
    
    """
    
    def __init__(self,TimeWindow:int=24,NumberOfSample:int=numpy.nan):
        """INITIALIZATION. See class description for documentation"""
        
        counts = pandas.DataFrame() #dfGrouped.apply(numpy.median)
        sums = pandas.DataFrame() #dfGrouped.apply(numpy.median)
        maxscore = pandas.DataFrame() #dfGrouped.apply(numpy.median)
        minscore = pandas.DataFrame() #dfGrouped.apply(numpy.median)
        
        self.iTimeWindow = TimeWindow
        self.iNumberOfSample = NumberOfSample
        self.counts = counts
        self.sums = sums
        self.maxscore = maxscore
        self.minscore = minscore
        
        return
    
    def __call__(self,dfAdd):
        """Execute aggregation. See class description for documentation
        
        The purpose of :py:func:`aggregate.__call__` is to process any number of check results and aggregate them into two series. The first series consists of the number of samples with value True in the column 'DateTime' of the added dataframe within time windows of specified length. The second series consists of the number of samples with value True in every column of the added dataframe (except the 'DateTime' column). Each call increments the number of samples.
        
        Parameters
        ----------
        dfAdd : Dataframe that needs to be aggregated. Must at least have the columns:
        
        * 'DateTime', consisting of datetimes (class: numpy.datetime64)
        
        * 'DateTimeCheck', consisting of booleans (class: bool)
        
        * 'Score', consisting of numeric values between 0 and 7
        
        Returns
        -------
        summarystats : A dataframe with summary statistics arranged per time window
        
        """
        
        TimeWindow = self.iTimeWindow 
        
        dfAddDTOK = dfAdd[dfAdd['DateTimeCheck']]
        dfAddDTOK = multiindex(dfAddDTOK)
        Time = dfAddDTOK.index.get_level_values('Time') 
        dfAddDTOK['Timeslot'] = [int(numpy.floor(v.hour/TimeWindow)) for v in Time]
        
        # ===========================================================================
        # Count and add number of samples where all checks are true:
        tf = dfAddDTOK[ numpy.setdiff1d(dfAddDTOK.columns,['DateTime', 'Timeslot','Score'])].all(axis=1)
        dfAddALLOK = dfAddDTOK[tf]
        dALL = numpy.array([dfAddALLOK.index.get_level_values('Date'),dfAddALLOK['Timeslot'].values,dfAddALLOK['Score'].values]).T
        go = pandas.DataFrame(data=dALL,columns=['Date', 'TimeSlot','Score']).groupby(['Date', 'TimeSlot'])
        
        maxADD = go.Score.apply(numpy.max)
        minADD = go.Score.apply(numpy.min)
        sumsADD = go.Score.apply(numpy.sum)
        countsADD = go.Score.size()
        
        if self.sums.shape[0]==0:
            sums = sumsADD
            counts = countsADD
            maxscore = maxADD
            minscore = minADD
        else:
            sums = self.sums.add(sumsADD,fill_value =0)
            counts = self.counts.add(countsADD,fill_value =0)
            maxscore = self.maxscore.combine(maxADD, numpy.fmax)
            minscore = self.minscore.combine(minADD, numpy.fmin)
        
        self.counts = counts
        self.sums = sums
        self.maxscore = maxscore
        self.minscore = minscore
        meanscore = sums.values/counts.values
        self.meanscore = meanscore
        
        df = numpy.array([sums.index.get_level_values('Date'),sums.index.get_level_values('TimeSlot'),\
                         sums.values,counts.values,minscore.values,maxscore.values,meanscore]).T
        #print(df)
        summarystats = pandas.DataFrame(data=df,\
                                        columns=['Date', 'TimeSlot','ScoreSum','ScoreCount','ScoreMin','ScoreMax','ScoreMean']) 
        
        summarystats.index = pandas.MultiIndex.from_arrays([summarystats['Date'], summarystats['TimeSlot']], names=['Date','TimeSlot'])
        summarystats.drop('Date', axis=1, inplace=True)
        summarystats.drop('TimeSlot', axis=1, inplace=True)
        summarystats.drop('ScoreSum', axis=1, inplace=True)
        summarystats.drop('ScoreCount', axis=1, inplace=True)
        
        self.summarystats = summarystats
        
        return summarystats, go
    
    def chartbar(self):
        """Generates a bar chart of the aggregated typicality scores, black for minimum score, red for mean score, and blue for maximum score.
        
        Parameters
        ----------
        
        Returns
        -------
        fig : Figure object
        ax : Axes object
        
        """
        
        iTimeWindow = self.iTimeWindow
        summarystats = self.summarystats
        
        date = summarystats.index.get_level_values('Date')
        Xlim= [date[0], date[-1]+datetime.timedelta(days=1)]
        
        rScoreMin = summarystats['ScoreMin']
        rScoreMin1 = oneindex(rScoreMin,iTimeWindow=iTimeWindow).astype(float)
        rScoreMin1Fill = rScoreMin1.resample(str(iTimeWindow)+'H',\
                                         loffset=pandas.Timedelta(hours=iTimeWindow)/2).min() #.fillna(0)
        
        rScoreMax = summarystats['ScoreMax']
        rScoreMax1 = oneindex(rScoreMax,iTimeWindow=iTimeWindow).astype(float)
        rScoreMax1Fill = rScoreMax1.resample(str(iTimeWindow)+'H',\
                                       loffset=pandas.Timedelta(hours=iTimeWindow)/2).max() #.fillna(0)

        rScoreMean = summarystats['ScoreMean']
        rScoreMean1 = oneindex(rScoreMean,iTimeWindow=iTimeWindow).astype(float)
        rScoreMean1Fill = rScoreMean1.resample(str(iTimeWindow)+'H',\
                                       loffset=pandas.Timedelta(hours=iTimeWindow)/2).mean() #.fillna(0)
        
        x = [rScoreMax1Fill.index,rScoreMean1Fill.index,rScoreMin1Fill.index,]
        y = [rScoreMax1Fill.values,rScoreMean1Fill.values,rScoreMin1Fill.values]
        
        Ylim = [0,7.01]
        Yticks =numpy.arange(0,8)
        
        fig,ax = makechartbar(x,y,Xlim=Xlim,Ylim=Ylim,Yticks=Yticks,iTimeWindow=iTimeWindow,rTarget=numpy.nan,Ylabel='Score [-]')
            
            
        return fig, ax
    
    def chartstem(self):
        """Generates a stem plot of the aggregated typicality scores, black for minimum score, red for mean score, and blue for maximum score.
        
        Parameters
        ----------
        
        Returns
        -------
        fig : Figure object
        ax : Axes object
        
        """
        
        iTimeWindow = self.iTimeWindow
        summarystats = self.summarystats
        
        date = summarystats.index.get_level_values('Date')
        Xlim = [ date[0], date[-1]+datetime.timedelta(days=1) ]
        
        rScoreMin = summarystats['ScoreMin']
        rScoreMin1 = oneindex(rScoreMin,iTimeWindow=iTimeWindow).astype(float)
        rScoreMin1Fill = rScoreMin1.resample(str(iTimeWindow)+'H',\
                                         loffset=pandas.Timedelta(hours=iTimeWindow)/2).min() #.fillna(0)
        
        rScoreMax = summarystats['ScoreMax']
        rScoreMax1 = oneindex(rScoreMax,iTimeWindow=iTimeWindow).astype(float)
        rScoreMax1Fill = rScoreMax1.resample(str(iTimeWindow)+'H',\
                                       loffset=pandas.Timedelta(hours=iTimeWindow)/2).max() #.fillna(0)

        rScoreMean = summarystats['ScoreMean']
        rScoreMean1 = oneindex(rScoreMean,iTimeWindow=iTimeWindow).astype(float)
        rScoreMean1Fill = rScoreMean1.resample(str(iTimeWindow)+'H',\
                                       loffset=pandas.Timedelta(hours=iTimeWindow)/2).mean() #.fillna(0)        
        
        x = [rScoreMax1Fill.index,rScoreMean1Fill.index,rScoreMin1Fill.index,]
        y = [rScoreMax1Fill.values,rScoreMean1Fill.values,rScoreMin1Fill.values]
        
        Ylim = [0,7.01]
        Yticks =numpy.arange(0,8)
        
        fig,ax = makechartstem(x,y,Xlim=Xlim,Ylim=Ylim,Yticks=Yticks,iTimeWindow=iTimeWindow,rTarget=numpy.nan,Ylabel='Score [-]')
        
        return fig, ax
    
    def chartheat(self,mode:str='min'):
        """Generates a calendar heatmap of the daily typicality score.
        
        Parameters
        ----------
        
        mode : [optional, default='min'] String defining the data that are visualized. 'min' - minimum score; 'max' - maximum score; or 'mean' - mean score.
        
        Returns
        -------
        fig : Figure object
        ax : Axes object
        
        """
        
        iTimeWindow = self.iTimeWindow
        summarystats = self.summarystats
        
        date = summarystats.index.get_level_values('Date')
        Ylim= [date[0], date[-1]+datetime.timedelta(days=1)]
        
        if mode=='min':
            rScore = summarystats['ScoreMin']
            rScoreONE = oneindex(rScore,iTimeWindow=iTimeWindow).astype(float)
            rScoreONEfill = rScoreONE.resample('D').min() #.fillna(0)
        elif mode=='max':
            rScore = summarystats['ScoreMax']
            rScoreONE = oneindex(rScore,iTimeWindow=iTimeWindow).astype(float)
            rScoreONEfill = rScoreONE.resample('D').max() #.fillna(0)
        elif mode=='mean':
            rScore = summarystats['ScoreMean']
            rScoreONE = oneindex(rScore,iTimeWindow=iTimeWindow).astype(float)
            rScoreONEfill = rScoreONE.resample('D').mean() #.fillna(0)
        else:
            raise SyntaxError('ERROR - Unknown mode')
            
        events = pandas.Series(rScoreONEfill.values, index=rScoreONEfill.index)
        events.dropna(inplace=True)
        
        fig, ax = makechartheat(events,mode='sequential',maximumvalue=7)
            
        return fig, ax

class typicalitydisplay:
    """
    :py:func:`typicalitydisplay` generates a dedicated figure to track the typicality check density model and produced typicality scores.
    
    Using the class
    ---------------
    
    Parameters
    ----------
    check : typicality check object
    memorylength : [optional, default=1024] number of samples to show
    density : [optional, default=[]] density score(s)
    value : [optional, default=[]] sensor value(s)
    score : [optional, default=[]] score value(s), length has to match value 
    figsize : [optional, default=(4,4)] size of the produced figure
    dpi : [optional,default=200] resolution of the produced figure
    
    Returns
    -------
    typicalitydisplay : Object describing the produced figure and axes
    
    """
    
    def __init__(self,check,*,memorylength=1024,density=[],value=[],score=[],figsize=(4,4),dpi=200):
        
        
        if len(value)==len(score):
            pass
        else:
            raise SyntaxError('ERROR - Length of value and score is not the same')
        
        if len(value)==0:
            value = numpy.nan*numpy.zeros(memorylength,dtype='float')
            score = numpy.nan*numpy.zeros(memorylength,dtype='float')
            density = numpy.nan*numpy.zeros(memorylength,dtype='float')
        
        hFig,hAx = plt.subplots(2,2, figsize=figsize,dpi=dpi)
        
        self.hFig = hFig
        self.hAx = hAx
        cmap = mapsequential()
        
        hScat00 = hAx[0,0].scatter(x=numpy.flipud(range(len(value))),\
                                   y=numpy.flipud(value),\
                                   c=numpy.flipud(score),\
                                   cmap=cmap,\
                                   marker='.',linestyle='None', vmin=0, vmax=7)
        
        hScat10 = hAx[1,0].scatter(x=numpy.flipud(range(len(value))),\
                                   y=numpy.flipud(score),\
                                   c=numpy.flipud(score),\
                                   cmap=cmap,\
                         marker='.',linestyle='None', vmin=0, vmax=7)
        
        hScat11 = hAx[1,1].scatter(x=density,\
                                   y=score,\
                                   c=numpy.flipud(score),\
                                   cmap=cmap,\
                                   marker='.',linestyle='None', vmin=0, vmax=7)
        
        hAx[0,0].set_xlim([-memorylength,0])
        hAx[0,0].set_ylim([check.rMinObs,check.rMaxObs])
        hAx[0,0].xaxis.set_label_position('top') 
        hAx[0,0].xaxis.tick_top()
        hAx[0,0].set_ylabel('feature value')
        
        hAx[0,1].plot(check.rWeight,check.rValue,color='k', marker='.')
        hAx[0,1].xaxis.set_label_position('top') 
        hAx[0,1].xaxis.tick_top()
        hAx[0,1].yaxis.set_label_position('right') 
        hAx[0,1].yaxis.tick_right()
        #hAx[0,1].set_ylim([check.rMinObs,check.rMaxObs])
        
        hAx[1,0].set_xlim([-memorylength,0])
        hAx[1,0].set_ylim([0,7])
        hAx[1,0].set_xlabel('sample number')
        hAx[1,0].set_ylabel('score')
        hAx[1,0].set_yticks(numpy.arange(0,7))
        
        hAx[1,1].set_ylim([0,7])
        hAx[1,1].yaxis.set_label_position('right') 
        hAx[1,1].yaxis.tick_right()
        hAx[1,1].set_xlabel('density')
        hAx[1,0].set_yticks(numpy.arange(0,7))
        
        self.hScat00 = hScat00
        self.hScat10 = hScat10
        self.hScat11 = hScat11
        
        self.memorylength =memorylength
        
        self.value = value
        self.score = score
        self.density = density
        
        hFig.canvas.draw()
        time.sleep(0.001)
        
        return
    
    def __call__(self,check,*,density=[],value=[],score=[],plotupdate=True):
        """Update figure
        
        The purpose of :py:func:`typicalitydisplay.__call__` is to update the typicalitydisplay figure.
        
        Parameters
        ----------
        check : typicality check object
        density : [optional, default=[]] density score(s)
        value : [optional, default=[]] sensor value(s)
        score : [optional, default=[]] score value(s), length has to match value 
        plotupdate : [default=True] boolean indicating whether figure panels should be updated (True) or only the internally stored data (False)
    
        Returns
        -------
        
        """
        
        if len(value)==len(score):
            if True:
                
                value = numpy.append( self.value, value )
                score = numpy.append( self.score, score )
                density = numpy.append( self.density, density )
                value = value[-self.memorylength:]
                score = score[-self.memorylength:]
                density = density[-self.memorylength:]
                
                self.density = density
                self.value = value
                self.score = score
                
                if plotupdate:
                    
                    hFig       = self.hFig
                    hAx        = self.hAx
                    
                    hScat00 = self.hScat00 
                    hScat10 = self.hScat10 
                    hScat11 = self.hScat11 
                    
                    x =-numpy.arange(len(value))
                    y = numpy.flipud(value)
                    z = numpy.flipud(score)
                    q = numpy.flipud(density)
                    
                    hScat00.set_offsets(numpy.array([x,y]).T)
                    hScat00.set_array(z)
                    hScat10.set_offsets(numpy.array([x,z]).T)
                    hScat10.set_array(z)
                    hScat11.set_offsets(numpy.array([q,z]).T)
                    hScat11.set_array(z)
                    
                    if check.rMinObs<check.rMaxObs:
                        hAx[0,0].set_ylim([check.rMinObs,check.rMaxObs])
                        hAx[0,1].set_ylim([check.rMinObs,check.rMaxObs])
                    
                    hAx[0,1].lines[0].set_ydata(check.rValue)
                    hAx[0,1].lines[0].set_xdata(check.rWeight)
                    rLIM = numpy.log2( numpy.maximum(1/len(check.rWeight),numpy.max( check.rWeight )) )
                    hAx[0,1].set_xlim([0,2**numpy.ceil( rLIM ) ] )
                    
                    hAx[1,0].set_ylim([0,7])
                    
                    hAx[1,1].set_xlim([0,2**numpy.ceil( rLIM ) ] )
                    self.hScat00 = hScat00
                    self.hScat10 = hScat10
                    self.hScat11 = hScat11
                    
                    hFig.canvas.draw()
                    time.sleep(0.001)
                    
        else:
            raise SyntaxError('ERROR - Length of value and score is not the same')
        
        return
        
class typicalitycheck:
    """
    The :py:func:`typicalitycheck` executes two tasks:
    (1) evaluate the degree to which a new sample is similar to historical values
    (2) update the density model describing the set of historical values
    
    Parameters
    ----------
    MinimumFeasibleValue : Minimal value for the signal which is considered ok
    MaximumFeasibleValue : Maximal value for the signal which is considered ok
    strTreatment : [optional, default = 'digital'] string specifying how the density model is defined: 'digital' means that a frequency for every processed value is defined. This is recommended for most raw sensor signals, as the number of unique feasible values is limited due to digitization. 'analog' means that the signal is treated as if it is an analog signal. This is recommended when (a) memory requirements are an issue and (b) when the analyzed signal is already processed (e.g., filtered), in which case the number of feasible values can increase dramatically.
    iSetupNumberOfBins : [optional, default=2048] Number of bins (only valid for analog treatment)
    
    Returns
    -------
    typicalitycheck : Object describing the typicality check
    
    """
    
    def __init__(self,MinimumFeasibleValue,MaximumFeasibleValue,*,Treatment = 'digital',iSetupNumberOfBins = 2**16):
        """Initialization. See class description for documentation"""
        
        rMin    = float(MinimumFeasibleValue)
        rMax    = float(MaximumFeasibleValue)
        
        self.rMin = rMin
        self.rMax = rMax
        self.rMinObs = rMin
        self.rMaxObs = rMax
        self.strTreatment = Treatment
        if Treatment=='analog':
            rWeight           = numpy.zeros(iSetupNumberOfBins+1)
            rValue            = numpy.linspace(rMin,rMax,iSetupNumberOfBins+1)
            iNumberTrainingSamples = 0
            self.iNumberTrainingSamples = iNumberTrainingSamples
        elif Treatment=='digital':
            rWeight           = numpy.zeros(2,dtype='int32')
            rValue            = [rMin,rMax]
        else:
            raise SyntaxError('ERROR - value %s for Treatment is not supported' % Treatment)
        
        self.rWeight = rWeight
        self.rValue = rValue
        
        return
    
    def __call__(self,Values,*,Mode='Sequential',NewSample=True):
        """Apply typicality check
        
        The purpose of :py:func:`typicalitycheck.__call__` is to 
        (a) evaluate whether numeric value(s) is/are typical relative to a historical data set, and 
        (b) update a density model describing historical data
        
        Parameters
        ----------
        Values : Sensor signal value(s) to be inspected
        Mode : [optional, default='Sequential'] Indicates whether the density model is based on previous samples only ('Sequential');  on all samples ('Batch'); or trained on all data but not applied ('Train').
        NewSample : [optional, default=True] Boolean indicating whether density model should be updated with provided data (True) or only applied (False)
    
        Returns
        -------
        Score : A value between 0 and 7 indicating the typicality of the inspected sensor signal value (0: never seen; 1: extremely rare; 2: rare; 3: somewhat rare; 4: somewhat frequent; 5: frequent; 6: very frequent; 7: extremely frequent)
        Density : Estimated density
        
        """
        
        rValues = numpy.float64(Values)
        Score = numpy.nan*numpy.ones(len(rValues))
        Density = numpy.nan*numpy.ones(len(rValues))
        
        if Mode=='Sequential':
            
            for i, rVal in enumerate(rValues):
                if NewSample:
                    self.updateweights(rVal)
                Score[i],Density[i]= self.score(rVal)
                
        elif Mode=='Batch':
            
            if NewSample:
                self.updateweights(rValues)
                
            for i, rVal in enumerate(rValues):
                Score[i],Density[i]= self.score(rVal)
                
        elif Mode=='Train':
            
            if NewSample:
                self.updateweights(rValues)
                
        else:
            raise SyntaxError('ERROR - value %s for Mode is not supported' % Mode)
        
        return Score, Density
    
    def score(self,rTestValue):
        """:py:func:`score` computes the typicality score
        
        Parameters
        ----------
        rTestValue : Sensor signal value(s) to be checked
    
        Returns
        -------
        Score : A value between 0 and 7 indicating the typicality of the inspected sensor signal value (0: never seen; 1: extremely rare; 2: rare; 3: somewhat rare; 4: somewhat frequent; 5: frequent; 6: very frequent; 7: extremely frequent)
        Density : Estimated density
        
        """
        
        rWeight = self.rWeight.copy() # make copy since weights are updated internally to compute out-of-bag score but should not be saved as such
        
        # for vizualization purposes: update range capturing observed values + tested value
        if numpy.logical_and(numpy.isreal(rTestValue),numpy.logical_not(numpy.isnan(rTestValue))):
            self.rMinObs = numpy.minimum(self.rMinObs,rTestValue)
            self.rMaxObs = numpy.maximum(self.rMaxObs,rTestValue)
                
        if self.strTreatment=='digital':
            xInHistory            = numpy.isin(self.rValue,rTestValue,assume_unique=True)
            if numpy.any(xInHistory):
                
                # ====================================================================================
                # I. MODIFYING WEIGHTS TO REMOVE EFFECT OF HAVING ADDED TEST SAMPLE ALREADY
                # ====================================================================================
                rWeight[xInHistory] = rWeight[xInHistory]-1
                
                # ====================================================================================
                # II. COMPUTE DENSITY AT TEST POINT    
                # ====================================================================================
                rWeight = numpy.maximum(rWeight,0)
                
                # ====================================================================================
                # III. COMPUTE FRACTION OF MASS WITH EQUAL OR HIGHER DENSITY
                # ====================================================================================
                if numpy.sum(rWeight)==0:
                    rFraction=1
                    rDensity=0
                else:
                    rDensity = rWeight[xInHistory]
                    rFraction = ( numpy.sum( rWeight[rWeight>rDensity]) )/numpy.sum(rWeight)
            else:
                rFraction=1
                rDensity=0
                
        elif self.strTreatment=='analog':
            
            if (self.rMin<=rTestValue) and (rTestValue<=self.rMax):
                iN0  = self.iNumberTrainingSamples # number of processed samples
                
                if iN0<=1:
                    # ====================================================================================
                    # I-III. COMPUTE FRACTION OF MASS WITH DENSITY EQUAL TO OR HIGHER THAN POINT DENSITY 
                    # ====================================================================================
                    rFraction=1
                    rDensity=0
                else:
                    
                    rValue =self.rValue
                    iValue = numpy.arange(len(self.rValue)) 
                    
                    if (rTestValue==self.rMin):
                        
                        # ====================================================================================
                        # I. MODIFYING WEIGHTS TO REMOVE EFFECT OF HAVING ADDED TEST SAMPLE ALREADY:
                        # ====================================================================================
                        # weight modification
                        rWeight[0] = rWeight[0]-1/iN0
                        # sanity measures: ensure non-negativity + normalize:
                        rWeight  = numpy.maximum(rWeight,0)
                        rWeight = rWeight/numpy.sum(rWeight)
                        
                        # ====================================================================================
                        # II. COMPUTE DENSITY AT TEST POINT    
                        # ====================================================================================
                        rDensity = rWeight[0] 
                        
                    else:
                        
                        # ====================================================================================
                        # I. MODIFYING WEIGHTS TO REMOVE EFFECT OF HAVING ADDED TEST SAMPLE ALREADY:
                        # ====================================================================================
                        # weight modification
                        xPairComparison   = rTestValue<=rValue
                        iBin         = numpy.argmax(xPairComparison)-1
                        
                        rLeft             = rValue[iBin]
                        rRight            = rValue[iBin+1]
                        rMassLeft         = rRight-rTestValue
                        rMassRight        = rTestValue-rLeft
                        rMassTotal        = rMassLeft+rMassRight
                    
                        # normalize to total mass of 1
                        rMassLeft = rMassLeft/rMassTotal
                        rMassRight = rMassRight/rMassTotal
                    
                        rWeight[iBin]   = rWeight[iBin]-rMassLeft/iN0
                        rWeight[iBin+1] = rWeight[iBin+1]-rMassRight/iN0
                        
                        # sanity measures: ensure non-negativity + normalize:
                        rWeight  = numpy.maximum(rWeight,0)
                        rWeight = rWeight/numpy.sum(rWeight)
                        
                        # ====================================================================================
                        # II. COMPUTE DENSITY AT TEST POINT    
                        # ====================================================================================
                        rDensity = rWeight[iBin]+(rWeight[iBin+1]-rWeight[iBin])*(rTestValue-rLeft)/(rRight-rLeft)
                        
                    # ====================================================================================
                    # III. COMPUTE FRACTION OF MASS WITH EQUAL OR HIGHER DENSITY
                    # ====================================================================================
                    rMassTotal=numpy.sum(rWeight)  
                    rIntegral = IntegratePWL(x=iValue,y=rWeight,offset=rDensity)
                    
                    rFraction = rIntegral/rMassTotal 
            else:
                # value outside of feasible region
                rFraction=1
                rDensity=0
            pass
        else:
            raise SyntaxError('ERROR - value %s for Treatment is not supported' % Treatment)
        
        # ====================================================================================
        # IV. COMPUTE SCORE
        # ====================================================================================
        if rFraction<=0:
            Score = 7
        elif rFraction>=1:
            Score = 0
        else:
            logFraction = numpy.log10(1-rFraction)
            Score = 7+logFraction
            # make sure the score is non-negative, by apply Elliot soft-sign for scores below 1:
            if Score<1:
                Score = 1 - (1-Score)/(1+numpy.abs(1-Score))
        
        Density = rDensity
        
        return Score, Density
        
    def updateweights(self,rNewValue):
        """:py:func:`updateweights` updates the weights defining the applied density model
        
        Parameters
        ----------
        rNewValue : Sensor signal value(s) to be added to density model
    
        Returns
        -------
        
        """
        
        rWeight                  = self.rWeight 
        rValue                   = self.rValue 
        rMin                      = self.rMin
        rMax                      = self.rMax
        strTreatment              = self.strTreatment
        
        # Ignore values that are not real or outside of feasible limits
        xSelect                   = numpy.logical_and(numpy.isreal(rNewValue),numpy.logical_not(numpy.isnan(rNewValue)))
        rNewValue                 = rNewValue[xSelect]
        xWithinLimits             = numpy.logical_and(rMin<=rNewValue,\
                                                   rNewValue<=rMax) 
        rNewValueWithinLimits = rNewValue[xWithinLimits]
        
        if self.strTreatment=='digital':
            
            # Digital treatment: rWeights keeps track of the number of times each value in rValues has been recorded before.
            
            if len(rNewValueWithinLimits)>0:
                
                rValueAdd, iCountAdd    = numpy.unique(rNewValueWithinLimits, return_counts=True)
            
                iNumberValueAdd          = len(rValueAdd)
                rWeightAdd               = numpy.zeros(iNumberValueAdd)
        
                rValue         = numpy.append(rValue,rValueAdd)
                rWeight        = numpy.append(rWeight,rWeightAdd)
        
                rValue, iIndex = numpy.unique(rValue, return_index=True)
                rWeight        = rWeight[iIndex]
            
                xAdd            = numpy.isin(rValue,rValueAdd)
                rWeight[xAdd] += iCountAdd 
                
                self.rValue = rValue
                self.rWeight = rWeight
                
        elif self.strTreatment=='analog':
            
            # Analog treatment: rWeights keeps track of piece-wise linear frequency polygon
            
            if len(rNewValueWithinLimits)>0:
                
                iN0  = self.iNumberTrainingSamples # number of previously processed samples
                iN1  = len(rNewValueWithinLimits) # number of samples that are added
                
                # Discount weights collected so far
                rWeight                  = rWeight*iN0/(iN0+iN1)
                
                # Account for values exactly on lower limit, i.e. the left edge of the leftmost bin
                xAtLowerLimit     = (rNewValueWithinLimits==self.rMin)
                xIndex            = rValue==self.rMin
                rWeight[xIndex]   += numpy.sum(xAtLowerLimit)/(iN0+iN1)
                
                # Add weights with remaining values
                rNewValueAboveMinimum = rNewValueWithinLimits[numpy.logical_not(xAtLowerLimit)]
                
                xPairComparison   = rNewValueAboveMinimum[:,None]<=rValue[None,:]
                iBinIndex         = numpy.argmax(xPairComparison,axis=1)-1
            
                rLeft             = rValue[iBinIndex]
                rRight            = rValue[iBinIndex+1]
                rMassRight        = (rNewValueAboveMinimum-rLeft)/(rRight-rLeft)
                
                for iBin in numpy.unique(iBinIndex):
                    iBinTotalMass    = numpy.sum(iBinIndex==iBin)
                    rBinMassRight    = numpy.sum(rMassRight[iBinIndex==iBin])
                    rBinMassLeft     = iBinTotalMass-rBinMassRight
                    
                    rWeight[iBin]   = rWeight[iBin]+rBinMassLeft/(iN0+iN1)
                    rWeight[iBin+1] = rWeight[iBin+1]+rBinMassRight/(iN0+iN1)
                
                # normalize:
                rWeight = rWeight/numpy.sum(rWeight)
                
                self.iNumberTrainingSamples = iN0+iN1
                self.rWeight = rWeight
                
            pass
        else:
            raise SyntaxError('ERROR - value %s for Treatment is not supported' % Treatment)
        
        # for vizualization purposes: compute range of observed values
        if numpy.any(self.rWeight>0):
            self.rMinObs = numpy.min(self.rValue[self.rWeight>0])
            self.rMaxObs = numpy.max(self.rValue[self.rWeight>0])
        
        return
    