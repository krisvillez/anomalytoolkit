#!/usr/bin/env python

import datetime
import matplotlib.dates  as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy
import pandas
from basic.visual import makechartbar
from basic.visual import makechartstem
from basic.visual import makechartheat
from basic.visual import multiindex
from basic.visual import oneindex
from basic.visual import mapsequential

class aggregate:
    """
    :py:func:`aggregate` generates an instance of the aggregated production check. This aggregation is used to summarize and visualize the results of basic production checks (of the :py:func:`datetimecheck` and :py:func:`numericcheck` classes).
    
    Parameters
    ----------
    TimeWindow : The time window to aggregate over, expressed in hours. Must be an integer equal to 1, 2, 3, 4, 6, 8, 12, or 24.
    NumberOfSample : [optional] The number of samples expected within the specified time window.
    
    Returns
    -------
    aggregate : Object describing the aggregated data
    
    """
    def __init__(self,TimeWindow:int=24,NumberOfSample:int=numpy.nan):
        """Initialize :py:func:`aggregate`. See class description for documentation"""
        
        iCount = pandas.DataFrame(columns=['Date', 'TimeSlot']).groupby(['Date', 'TimeSlot']).size()
        
        self.iTimeWindow = TimeWindow
        self.iNumberOfSample = NumberOfSample
        self.iCountCheckDateTime = iCount
        self.iCountAll = iCount
        
        return
    
    def __call__(self,dfAdd):
        """The purpose of :py:func:`aggregate.__call__` is to process any number of check results and aggregate them into two series. The first series consists of the number of samples with value True in the column 'DateTime' of the added dataframe within time windows of specified length. The second series consists of the number of samples with value True in every column of the added dataframe (except the 'DateTime' column). Each call increments the number of samples.
        
        Parameters
        ----------
        dfAdd : Dataframe that needs to be aggregated. Must at least have the columns:
        
        * 'DateTime', consisting of datetimes (class: numpy.datetime64)
        
        * 'DateTimeCheck', consisting of booleans (class: bool)
        
        and can have any additional columns containing booleans (class: bool)
    
        Returns
        -------
        iCountCheckDateTime : Number of added positive checks in column 'DateTimeCheck' (integer)
        iCountAll : Number of positive checks in all columns (integer)
        
        """
        
        TimeWindow = self.iTimeWindow 
        iCountCheckDateTime = self.iCountCheckDateTime
        iCountAll = self.iCountAll
        
        dfAddDTOK = dfAdd[dfAdd['DateTimeCheck']]
        dfAddDTOK = multiindex(dfAddDTOK)
        Time = dfAddDTOK.index.get_level_values('Time') 
        dfAddDTOK['Timeslot'] = [int(numpy.floor(v.hour/TimeWindow)) for v in Time]
        
        # ===========================================================================
        # Count and add number of samples with correct datetime:
        dDT = numpy.array([dfAddDTOK.index.get_level_values('Date'),dfAddDTOK['Timeslot'].values]).T
        iCountAddDT = pandas.DataFrame(data=dDT,columns=['Date', 'TimeSlot']).groupby(['Date', 'TimeSlot']).size()
        iCountCheckDateTime = iCountCheckDateTime.add(iCountAddDT,fill_value=0)
        
        # ===========================================================================
        # Count and add number of samples where all checks are true:
        tf = dfAddDTOK[ numpy.setdiff1d(dfAddDTOK.columns,['DateTime', 'Timeslot'])].all(axis=1)
        dfAddALLOK = dfAddDTOK[tf]
        dALL = numpy.array([dfAddALLOK.index.get_level_values('Date'),dfAddALLOK['Timeslot'].values]).T
        iCountAddALL = pandas.DataFrame(data=dALL,columns=['Date', 'TimeSlot']).groupby(['Date', 'TimeSlot']).size()
        iCountAll = iCountAll.add(iCountAddALL,fill_value=0)
        
        
        self.iCountCheckDateTime = iCountCheckDateTime
        self.iCountAll = iCountAll
        
        return iCountCheckDateTime, iCountAll
    
    def chartbar(self):
        """Generates a bar chart of the aggregated numbers of :py:func:`datetimecheck` results and :py:func:`numericcheck` results. Red color is used to indicate successful datetime checks, black for successful numericchecks, blue for the targeted number of successful checks.
        
        Parameters
        ----------
        
        Returns
        -------
        fig : Figure object
        ax : Axes object
        
        """
        
        iCountAll = self.iCountAll
        iCountCheckDateTime = self.iCountCheckDateTime
        iTimeWindow = self.iTimeWindow
        
        date = iCountAll.index.get_level_values('Date')
        Xlim = [date[0],date[-1]+datetime.timedelta(days=1)]
        
        iCountDT = oneindex(iCountCheckDateTime,iTimeWindow=iTimeWindow)
        iCountA = oneindex(iCountAll,iTimeWindow=iTimeWindow)
        iCountDTfill = iCountDT.resample(str(iTimeWindow)+'H',\
                                         loffset=pandas.Timedelta(hours=iTimeWindow)/2).sum().fillna(0)
        iCountAfill = iCountA.resample(str(iTimeWindow)+'H',\
                                       loffset=pandas.Timedelta(hours=iTimeWindow)/2).sum().fillna(0)
        
        x = [iCountDTfill.index,iCountAfill.index]
        y = [iCountDTfill.values,iCountAfill.values]
        
        iN = self.iNumberOfSample
        if (numpy.isnan(iN)):
            iN = numpy.ceil(numpy.max(iCountCheckDateTime.values)/10)

        Ylim = [0,float(iN)*1.05]
        Yticks =numpy.arange(11)*iN/10
        
        fig,ax = makechartbar(x,y,\
                              Xlim=Xlim,Ylim=Ylim,Yticks=Yticks,\
                              iTimeWindow=iTimeWindow,rTarget=self.iNumberOfSample,Ylabel='Number of samples [-]')
            
        return fig, ax
    
    def chartstem(self):
        """Generates a stem plot of the aggregated numbers of :py:func:`datetimecheck` results and :py:func:`numericcheck` results. Red color is used to indicate successful datetime checks, black for successful numericchecks, blue for the targeted number of successful checks.
        
        Parameters
        ----------
        
        Returns
        -------
        fig : Figure object
        ax : Axes object
        
        """
        
        iCountAll = self.iCountAll
        iCountCheckDateTime = self.iCountCheckDateTime
        iTimeWindow = self.iTimeWindow
        
        iN = self.iNumberOfSample
        if numpy.isnan(iN):
            iN = numpy.ceil(numpy.max(iCountCheckDateTime.values)/10)
       
        date = iCountAll.index.get_level_values('Date')
        Xlim = [date[0],date[-1]+datetime.timedelta(days=1)]
        
        iCountDT = oneindex(iCountCheckDateTime,iTimeWindow=iTimeWindow)
        iCountA = oneindex(iCountAll,iTimeWindow=iTimeWindow)
        iCountDTfill = iCountDT.resample(str(iTimeWindow)+'H',\
                                         loffset=pandas.Timedelta(hours=iTimeWindow)/2).sum().fillna(0)
        iCountAfill = iCountA.resample(str(iTimeWindow)+'H',\
                                       loffset=pandas.Timedelta(hours=iTimeWindow)/2).sum().fillna(0)
        
        x = [iCountDTfill.index,iCountAfill.index]
        y = [iCountDTfill.values,iCountAfill.values]
        Yticks =numpy.arange(11)*iN/10
        Ylim = [0,float(iN)*1.05]
        
        fig,ax = makechartstem(x,y,Xlim=Xlim,Ylim=Ylim,Yticks=Yticks,iTimeWindow=iTimeWindow,rTarget=numpy.nan,Ylabel='Number of Samples [-]')
            
        return fig, ax
    
    def chartheat(self,mode:str='all'):
        """Generates a calendar heatmap of the daily numbers of :py:func:`datetimecheck` results and :py:func:`numericcheck` results. Red color is used to indicate successful datetime checks, black for successful numericchecks, blue for the targeted number of successful checks.
        
        Parameters
        ----------
        
        mode : String defining the data that are visualized:
        
        * mode='all' or unspecified - number of all-positive checks
        * mode='datetime' - number of positive datetime checks
        * mode='ratio' - ratio of all-positive checks to the number of positive datetime checks are considered
        
        Returns
        -------
        fig : Figure object
        ax : Axes object
        
        """
        
        if mode=='datetime':
            iCount = self.iCountCheckDateTime
        elif mode=='ratio':
            iCount0 = self.iCountCheckDateTime
            iCount1 = self.iCountAll
            iCount = iCount1/iCount0
        else:
            iCount = self.iCountAll
            
        iTimeWindow = self.iTimeWindow
        iN = self.iNumberOfSample
        
        if numpy.isnan(iN):
            iN = numpy.ceil(numpy.max(iCount.values)/10)
       
        date = iCount.index.get_level_values('Date')
        
        iCountONE = oneindex(iCount,iTimeWindow=iTimeWindow)
        
        if mode=='ratio':
            iCountONEfill = iCountONE.resample('D').mean() #.fillna('NA')
            z = iCountONEfill.values
        else:
            iCountONEfill = iCountONE.resample('D').sum().fillna(0)
            iN = iN*24/iTimeWindow
            f = iCountONEfill.values/iN
            y = numpy.maximum(0,f-1)
            z = numpy.minimum(1,f)+y/(1+numpy.abs(y))
            
        events = pandas.Series(z, index=iCountONEfill.index)
        events.dropna(inplace=True)
        
        fig, ax = makechartheat(events,mode='divergent',maximumvalue=2)
            
        return fig, ax
    
class datetimecheck:
    """
    The :py:func:`datetimecheck` executes a check of the datetime format. 
    
    Parameters
    ----------
    dtFormat : String specifying the datetime format, e.g. 'dd-mm-yyyy HH:MM:SS'
    
    Returns
    -------
    datetimecheck : Object describing the datetime format check
    
    """
    
    
    def __init__(self,DateTimeFormat:str):
        """Initialize :py:func:`datetimecheck`. See class description for documentation"""
        
        if isinstance(DateTimeFormat,str):
            self.strDateTimeFormat = DateTimeFormat
        else:
            raise ValueError('Class datetimecheck expected DateTimeFormat to be a string.')
            
        return
    
    def __call__(self,DateTimeEntry:str):
        """Execute :py:func:`datetimecheck`
        
        The purpose of :py:func:`datetimecheck.__call__` is to check if a given string adheres to the predefined datetime format.
        
        Parameters
        ----------
        DateTimeEntry : String to be checked
    
        Returns
        -------
        DateTimeCheck : Result of check (boolean)
        DateTime : Datetime object (dt)
        
        """
        
        DateTime = [self.convertoneentry(entry) for entry in DateTimeEntry]
        DateTimeCheck = [(not str(entry) == "NaT") for entry in DateTime] 
        
        return DateTimeCheck, DateTime
    
    def convertoneentry(self,DateTimeEntry:str):
        """:py:func:`convertoneentry` attempts to converts a string into a numpy.datetime64.
        
        Parameters
        ----------
        DateTimeEntry : String to be converted
    
        Returns
        -------
        DateTime : converted datetime (as numpy.datetime64),'NaT' if failed
        
        """
        
        if isinstance(DateTimeEntry,str):
            try:
                DateTime = numpy.datetime64(datetime.datetime.strptime(DateTimeEntry, self.strDateTimeFormat))
            except:
                DateTime = numpy.datetime64('NaT')
        else:
            raise ValueError('Function datetimecheck.checkoneentry expected DateTimeEntry to be a string.')
        
        return DateTime
    
class numericcheck:
    """
    The :py:func:`numericcheck` executes a numeric format check. This check is used to verify if sensor readings (in string format) can be interpreted correctly as a number of a targeted format.
    
    Parameters
    ----------
    NumberFormat : String specifying the datetime format, e.g. 'dd-mm-yyyy HH:MM:SS'
    
    Returns
    -------
    numericcheck : Object describing the number format check
    
    """
    
    
    def __init__(self,NumberFormat:str='float'):
        """Initialize :py:func:`numericcheck`. See class description for documentation"""
        
        if isinstance(NumberFormat,str):
            self.strNumberFormat = NumberFormat
        else:
            raise ValueError('Class numericcheck expected ValueFormat to be a string.')
            
        return
    
    def __call__(self,NumberEntry:str):
        """Execute :py:func:`numericcheck`
        
        The purpose of :py:func:`datetimecheck.__call__` is to check if a given string adheres to the predefined number format.
        
        Parameters
        ----------
        NumberEntry : String to be checked
    
        Returns
        -------
        NumberCheck : Result of check (boolean)
        Number : numeric object (int if numericcheck.strNumberFormat=='int', float otherwise)
        
        """
        
        Number = [self.convertoneentry(entry) for entry in NumberEntry]
        NumberCheck = [(not numpy.isnan(entry) ) for entry in Number] 
        
        return NumberCheck, Number
    
    def convertoneentry(self,NumberEntry:str):
        """:py:func:`convertoneentry` attempts to converts a string into a number.
        
        Parameters
        ----------
        NumberEntry : String to be converted
    
        Returns
        -------
        Number : numeric object (int if numericcheck.strNumberFormat=='int', float otherwise)
        
        """
        
        if isinstance(NumberEntry,str):
            try:
                if (self.strNumberFormat=='int'):
                    Number = int(NumberEntry)
                else: # Assume float
                    Number = float(NumberEntry)
            except:
                Number = numpy.nan
            
        else:
            raise ValueError('Function numericcheck.checkoneentry expected NumberEntry to be a string.')
            
        return Number