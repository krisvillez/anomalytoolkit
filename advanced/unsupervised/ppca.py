#!/usr/bin/env python

import numpy as np

def decompose(Ytil,Center='no',Scale='no'):
    """
    :py:func:`decompose` applies centering, scaling, and singular value decomposition to compute a principal component analysis model with the maximum number of components.
    
    Parameters
    ----------
    Ytil : Data matrix, dimensions: #samples x #variables
    Center : [optional] Method for centering as a string, currently available: 'no', 'mean'
    Scale : [optional] Method for scaling as a string, currently available: 'no', 'unitvariance'
    
    Returns
    -------
    center : Location of center in original space
    scale : Applied scaling factors
    P : Eigenvectors as columns vectors, dimensions: #variables x #components
    Lambda : Vector of eigenvalues, dimensions: max. #components 
    Scores : Vector of scores, dimensions: #samples x max. #components 
    
    """
    
    I, J = np.shape(Ytil)
    
    # ===========================
    #        preprocessing
    # ===========================
    if Center=='mean':
        center = np.mean(Ytil,axis=0)
    elif Center=='no':
        center = np.zeros(J)
    else:
        raise ValueError('ERROR - value %s for Center is not supported' % Center)
        
    Ytil = Ytil-center
    
    if Scale=='unitvariance':
        scale = np.mean(Ytil**2,axis=0)
    elif Scale=='no':
        scale = np.ones(J)
    else:
        raise ValueError('ERROR - value %s for Center is not supported' % Center)
        
    Ytil = Ytil/scale
    
    
    # =========================================
    #        singular value decomposition
    # =========================================
    
    [U,S,Vh] = np.linalg.svd(Ytil,full_matrices=False)
    
    P = Vh.T
    """
    Kmax = np.min([I,J])-1
    U = U[:,:Kmax]
    P = P[:,:Kmax]
    S = S[:Kmax]
    """
    
    Lambda = S**2/I
    
    Scores = U*S
    
    return center, scale, P, Lambda, Scores

def build(center, scale, P, Lambda, K, I, J):
    """
    :py:func:`build` constructs a probabilistic principal component analysis (PPCA) model.
    
    Parameters
    ----------
    center : Location of center in original space
    scale : Applied scaling factors
    P : Eigenvectors as columns vectors, dimensions: #variables x #components
    Lambda : Vector of eigenvalues, dimensions: max. #components 
    K : #components
    I : #samples
    J : #variables
    
    Returns
    -------
    center : Location of center in original space
    scale : Applied scaling factors
    P : Eigenvectors as columns vectors, dimensions: #variables x #components
    LambdaK : Modified eigenvalues, dimensions: max. #components 
    sigma_noise : Estimate of the noise variance
    
    """
    
    LambdaK =Lambda.copy()
    Kmax = np.min([I,J])
    
    if K==Kmax:
        sigma_noise = 0
    else:
        sigma_noise = np.sum(LambdaK[K:])/(Kmax-K)
        LambdaK[K:] =sigma_noise
    
    return center, scale, P, LambdaK, sigma_noise

def test(Ytil,center, scale, P, LambdaK):
    """
    :py:func:`test` applies a probabilistic principal component analysis (PPCA) model to a data matrix.
    
    Parameters
    ----------
    Ytil : Test data matrix, dimensions: #samples x #variables
    center : Location of center in original space
    scale : Applied scaling factors
    P : Eigenvectors as columns vectors, dimensions: #variables x #components
    LambdaK : Vector of PPCA eigenvalues, dimensions: max. #components 
    
    Returns
    -------
    distance2 : Mahalanobis distance, dimensions: #samples
    ignorance : ignorance score, dimensions: #samples
    Xtil : principal scores, dimensions: #samples x max. #components 
    
    """
    
    J = np.shape(Ytil)[1]
    Jc = np.shape(center)[0]
    Js = np.shape(scale)[0]
    JP = np.shape(P)[0]
    
    if np.all(np.equal([Jc,Js,JP],J)):
        Ytil = Ytil-center
        Ytil = Ytil/scale
        Xtil = np.matmul(Ytil, P)
        Util = Xtil/np.sqrt(LambdaK)
        
        distance2 = np.sum(Util**2,axis=1)
        ignorance = +np.log(2*np.pi)*J/2+ np.sum( np.log(LambdaK))+distance2/2  ;
    else:
        raise ValueError('ERROR - Inconsistent dimensions of function inputs')
    
    return distance2, ignorance, Xtil

def crossvalidate(Ytil,Center='no',Scale='no',Kmax_input=np.nan):
    """
    :py:func:`crossvalidate` applies leave-one-out cross-validation to identify the optimal number of principal components in a probabilistic principal component analysis (PPCA) model.
    
    Parameters
    ----------
    Ytil : Data matrix, dimensions: #samples x #variables
    Center : [optional] Method for centering as a string, currently available: 'no', 'mean'
    Scale : [optional] Method for scaling as a string, currently available: 'no', 'unitvariance'
    Kmax_input : [optional] Maximum number of components to consider
    
    Returns
    -------
    ignorance : Ignorance scores for every candidate number of components from 0 to #Kmax_input, dimensions: 1+#Kmax_input
    
    """
    
    I, J = np.shape(Ytil)
    Kmax = int(np.nanmin([Kmax_input,I,J]))
    
    IGN = np.nan*np.ones([I,Kmax+1])
    
    for iVal in range(I):
        iCal = np.setdiff1d(range(I),iVal)
        
        YtilCal = Ytil[iCal,:]
        YtilVal = Ytil[iVal,:]
        if len(np.shape(YtilVal))==1:
            YtilVal = YtilVal[None,:]
        
        center, scale, P, Lambda, Scores = decompose(YtilCal,Center,Scale)
    
        for Khat in range(Kmax+1):
            center, scale, P, LambdaK, sigma_noiseK = build(center, scale, P, Lambda, Khat, I, J)
            dist2, ign, scores = test(YtilVal,center, scale, P, LambdaK)
            IGN[iVal,Khat] = ign
    
    ignorance = np.mean(IGN,axis=0)
    
    return ignorance
    