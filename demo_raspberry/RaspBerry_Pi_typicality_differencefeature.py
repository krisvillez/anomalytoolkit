import os
import glob
import pandas
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
import datetime

# Load tools from the AnomalyToolKit
sys.path.append('..')
from basic.production import datetimecheck
from basic.production import numericcheck
from feature.rateofchange import pointwisedifference
from basic.typical import typicalitycheck
from basic.typical import typicalitydisplay

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
 
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
 
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f

plt.ion()

counter=0
# fig, ax =  plt.subplots(nrows=1,ncols=1)
# ax.plot(counter,np.nan,'k.')

CheckDT = datetimecheck(DateTimeFormat='%Y-%m-%d %H:%M:%S.%f')
CheckNUM = numericcheck(NumberFormat='float')
FeatureDIFF = pointwisedifference()
CheckTYP = typicalitycheck(MinimumFeasibleValue=-10,MaximumFeasibleValue=+10)

DispTYP = typicalitydisplay(CheckTYP,memorylength=300)

while True:
    counter += 1
    
    # get data
    rSensorReading = read_temp()
    strSampleDT = str(datetime.datetime.now())
    rSampleValue = str(rSensorReading[0])
    print([strSampleDT, rSensorReading[0]])

    # execute production checks
    xCheckDT,dtCheckDT = CheckDT(DateTimeEntry=[strSampleDT])
    xCheckVAL,rCheckVAL = CheckNUM(NumberEntry=[rSampleValue])
    print([xCheckDT,xCheckVAL])
    
    # compute feature
    rFeatDIFF = FeatureDIFF(rCheckVAL[0])

    # execute typicality check
    rScore,rDensity = CheckTYP([rFeatDIFF])
    print([rScore])
    
    # produce plot
    if np.mod(counter,5)==0:
        xDoPlot = True
    else:
        xDoPlot = False
    DispTYP(CheckTYP,value=[rFeatDIFF],score=rScore,density=rDensity,plotupdate=xDoPlot)
    # ax.plot(counter,rSensorReading[0],'ko')
    # fig.canvas.draw()
    time.sleep(0.001)
