ANOMALY TOOL KIT V0.2
=====================

INSTALLATION (Windows)
----------------------

1. Install Anaconda ( https://www.anaconda.com/ )
2. Get the AnomalyToolKit package:
	a. Download from GitLab: https://gitlab.com/krisvillez/anomalytoolkit 
	b. Unzip into a folder of your liking, e.g. C:/workshop/anomalytoolkit
3. Open command prompt (e.g. cmd.exe) and execute the following steps:
	a. Create virtual environment with this command:
		conda create -n ATK python=3.7 anaconda
	b. Install required packages with this pip command:
		pip install -r C:/G/code/anomalytoolkit\requirements.txt
3. [for demo/workshop use] Download the data set used for demonstration:
	a. Download the file from Zenodo at https://zenodo.org/record/3229195
	b. Unzip and place the folder a location of your liking, e.g.: C:/workshop/data/potential_rowB
4. You are ready!

TEST RUN
--------

To check if all is installed correctly, you can run one of the demos. These are Python notebooks and are located in the ./anomalytoolkit/demo folder. To do so:
1. Start Anaconda Navigator
2. Select the ATK environment
3. Open Jupyter notebooks and navigate to the ./anomalytoolkit/demo folder (e.g., C:/workshop/anomalytoolkit/demo )
4. Select one of the notebooks (.ipynb files) and run the cells

